// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

// 1 :
describe('The Traffic web site configuration page', () => {
	//tester que la page de configuration se charge bien
    it('successfully load configuration page', () => {
		cy.contains('Configuration').click()
        cy.visit('http://localhost:3000/#configuration ')
    })
})


// 2 :
describe('The Traffic web site configuration page 28 segments', () => {
	//tester que le réseau routier courant comprend bien 28 segments
    it('28 segments in segments list', () => {	
	cy.get('a[href="#configuration"]').click();  
	cy.get('form[data-kind="segment"]').should('have.length', 28)
    })
})


// 3 :
describe('The Traffic web site 0 vehicle available', () => {	
	//tester que le nombre de véhicules est pour le moment 0
    it('0 vehicle in vehiculs list', () => {	
	   cy.get('table').contains('No vehicle available')

    })
})

/*
// 4 :
describe('The Traffic web site change value speed', () => {    
	//modifier la valeur de vitesse du segment 5 à 30
    it('edit speed segment 5 ', () => {    
        cy.get('a[href="#configuration"]').click();  
        cy.get('input[name ="speed"][form ="segment-5"]').clear().type('30')
        cy.get('form[id=segment-5] > button').click()
        cy.contains('Close').click()
  })
  
    //vérifier les changements sur le serveur de l’API en exploitant la route ‘elements
    it('successfully edit in API ', () => {    
		cy.request('GET','http://127.0.0.1:4567/elements').as('elements')
		cy.get ('@elements').should((response)=>{expect (response.body['segments'][4]).to.have.property('speed',30)})        
  })   
})
*/


// 5 :
describe('The Traffic web site change capacite and duree', () => { 
	//Modifier le rond point pour qu’il puisse avoir une capacité de 4 et une durée de 15 secondes   
    it('edit capacity and duration roundabout  ', () => {    
        cy.get('a[href="#configuration"]').click();  
        cy.get('input[name ="capacity"][form="roundabout-31"]').clear().type('4')
        cy.get('input[name ="duration"][form="roundabout-31"]').clear().type('15')
        cy.get('form[name="roundabout-31"] > button').click()
        cy.contains('Close').click()
         })
         /*
	//vérifier les changements sur cote client
    it('successfully edit in interface ', () => {  
	cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control').should('contain','4') 
	cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control').should('contain','15') 
		  })
        */
    //vérifier les changements sur le serveur de l’API en exploitant la route elements
	it('successfully edit in API ', () => {  
		 cy.reload()  
		cy.request('GET','http://127.0.0.1:4567/elements').as('elements')
		cy.get ('@elements').should((response)=>{expect (response.body['crossroads'][2]).to.have.property('duration',15)})
		cy.get ('@elements').should((response)=>{expect (response.body['crossroads'][2]).to.have.property('capacity',4)})

    })
})


// 6 :
describe('The Traffic web site change orangeDuration greenDuration nextPassageDuration', () => {  
	//Modifier le feu d’identifiant 29  
    it('edit trafficlight-29', () => {    
        cy.get('a[href="#configuration"]').click();  
        cy.get('input[name ="orangeDuration"][form="trafficlight-29"]').clear().type('4')
		cy.get('input[name ="greenDuration"][form="trafficlight-29"]').clear().type('40')
		cy.get('input[name ="nextPassageDuration"][form="trafficlight-29"]').clear().type('8')
        cy.get('form[name="trafficlight-29"] > button').click()
        cy.reload()
		})
	 //vérifier les changements sur le serveur de l’API en exploitant la route elements
	it('successfully edit in API ', () => {  	
		cy.request('GET','http://127.0.0.1:4567/elements').as('elements')
		cy.get ('@elements').should((response)=>{expect (response.body['crossroads'][0]).to.have.property('orangeDuration',4)})
		cy.get ('@elements').should((response)=>{expect (response.body['crossroads'][0]).to.have.property('greenDuration',40)})
		cy.get ('@elements').should((response)=>{expect (response.body['crossroads'][0]).to.have.property('nextPassageDuration',8)})
    })
})


// 7 :
describe('The Traffic web site add 3 vehicls', () => {   
	//ajouter 3 vehicules 
    it('add 3 vehicls', () => {    
        cy.get('a[href="#configuration"]').click();  
        //premiere vehicle
        cy.get('input[name ="origin"][form="addVehicle"]').clear().type('5')
		cy.get('input[name ="destination"][form="addVehicle"]').clear().type('26')
		cy.get('input[name ="time"][form="addVehicle"]').clear().type('50')
		cy.get('form[name="addVehicle"] > button').click()
		cy.contains('Close').click()
		//deuxieme 
		cy.get('input[name ="origin"][form="addVehicle"]').clear().type('19')
		cy.get('input[name ="destination"][form="addVehicle"]').clear().type('8')
		cy.get('input[name ="time"][form="addVehicle"]').clear().type('200')
		cy.get('form[name="addVehicle"] > button').click()
		cy.contains('Close').click()
		//troisieme
		cy.get('input[name ="origin"][form="addVehicle"]').clear().type('27')
		cy.get('input[name ="destination"][form="addVehicle"]').clear().type('2')
		cy.get('input[name ="time"][form="addVehicle"]').clear().type('150')
		cy.get('form[name="addVehicle"] > button').click()
		cy.contains('Close').click()
		 })

	//vérifier les changements sur le serveur de l’API en exploitant la route vehicles
	it('successfully edit in API ', () => { 
		cy.request('GET','http://127.0.0.1:4567/vehicles').as('vehicles')        
		cy.get ('@vehicles').should((response)=>{expect (response.body['200.0'][0]).to.have.property('position',0)})
		cy.get ('@vehicles').should((response)=>{expect (response.body['150.0'][0]).to.have.property('position',0)})
		cy.get ('@vehicles').should((response)=>{expect (response.body['50.0'][0]).to.have.property('position',0)})
       })
      
	 //vérifier les changements sur l'interface client
    it('successfully edit in interface client ', () => 	{	
       	cy.contains('Simulation').click()
       		// premiere vehicule	
		cy.get('tbody > :nth-child(1) > :nth-child(2').should('contain', 200) 
		cy.get('tbody > :nth-child(1) > :nth-child(3)').should('contain', 0) 
		cy.get('tbody > :nth-child(1) > :nth-child(4)').should('contain', 0) 
		cy.get('tbody > :nth-child(1) > :nth-child(5)').should('contain', 'No step') 
	   		// deuxieme vehicule	
		cy.get('tbody > :nth-child(2) > :nth-child(2').should('contain', 150) 
		cy.get('tbody > :nth-child(2) > :nth-child(3)').should('contain', 0) 
		cy.get('tbody > :nth-child(2) > :nth-child(4)').should('contain', 0) 
		cy.get('tbody > :nth-child(2) > :nth-child(5)').should('contain', 'No step') 
		// troixieme vehicule	
		cy.get('tbody > :nth-child(3) > :nth-child(2').should('contain', 50) 
		cy.get('tbody > :nth-child(3) > :nth-child(3)').should('contain', 0) 
		cy.get('tbody > :nth-child(3) > :nth-child(4)').should('contain', 0) 
		cy.get('tbody > :nth-child(3) > :nth-child(5)').should('contain', 'No step') 
    })
})


// 8 :
describe('The Traffic web site simulation 120 seconds', () => {    
    it('evry vehicles in stop', () => {    
       	cy.contains('Simulation').click()
       	//tester que tous les véhicules sont bien à l’arrêts
       	cy.get('tbody > :nth-child(1) > :nth-child(5)').should('contain', 'No step') 
		cy.get('tbody > :nth-child(2) > :nth-child(5)').should('contain', 'No step') 
		cy.get('tbody > :nth-child(3) > :nth-child(5)').should('contain', 'No step') 
       	   })
    it('edit time and execute the simulation', () => {   
         //changer le temps et executer la simulation simultaion
        cy.get('.form-control').clear().type('120')  
        cy.get('.btn').click();
         })
     it('evry vehicles in stop except 1 vehicle', () => {   
		cy.wait(20000)	
		//Vérifier que seul le premier véhicule est en mouvement en fin de simulation
		cy.get('tbody > :nth-child(1) > :nth-child(5)').should('contain', 'No step') 
		cy.get('tbody > :nth-child(2) > :nth-child(5)').should('contain', 'No step') 
		cy.get('tbody > :nth-child(3) > :nth-child(5)').should('contain', 3)

    })
})


// 9 :
describe('The Traffic web site evry vehicles in stop', () => {    
	it('0 vehicle in vehiculs list', () => { 
		// ré-initialisér la simulation  et vérifier que les 3 véhicules ne sont plus présents
		 cy.reload()
		 cy.get('table').contains('No vehicle available')
	 })
	 //ajouter 3 vehicules
	it('add 3 vehicls', () => {   
		cy.get('a[href="#configuration"]').click(); 
		 //premiere vehicle
        cy.get('input[name ="origin"][form="addVehicle"]').clear().type('5')
		cy.get('input[name ="destination"][form="addVehicle"]').clear().type('26')
		cy.get('input[name ="time"][form="addVehicle"]').clear().type('50')
		cy.get('form[name="addVehicle"] > button').click()
		cy.contains('Close').click()
		//deuxieme 
		cy.get('input[name ="origin"][form="addVehicle"]').clear().type('19')
		cy.get('input[name ="destination"][form="addVehicle"]').clear().type('8')
		cy.get('input[name ="time"][form="addVehicle"]').clear().type('200')
		cy.get('form[name="addVehicle"] > button').click()
		cy.contains('Close').click()
		//troisieme
		cy.get('input[name ="origin"][form="addVehicle"]').clear().type('27')
		cy.get('input[name ="destination"][form="addVehicle"]').clear().type('2')
		cy.get('input[name ="time"][form="addVehicle"]').clear().type('150')
		cy.get('form[name="addVehicle"] > button').click()
		cy.contains('Close').click()
	})
	   it('edit time and execute simulation', () => {   
		   //changer temps et executer la simulation simultaion
		cy.contains('Simulation').click()
        cy.get('.form-control').clear().type('500')  
        cy.get('.btn').click();
            })
     it('evry vehicles in stop ', () => {   
		cy.wait(70000)
		  //verifier les 3 vehicles sont en stop en fin de simultion
		cy.get('tbody > :nth-child(1) > :nth-child(6)').should('contain', 'block') 
		cy.get('tbody > :nth-child(2) > :nth-child(6)').should('contain', 'block') 
		cy.get('tbody > :nth-child(3) > :nth-child(6)').should('contain', 'block')
    })
})


// 10 :
describe('The Traffic web site evry vehicles in stop', () => {    
  	it('0 vehicle in vehiculs list', () => { 
		// ré-initialisér la simulation  et vérifier que les 3 véhicules ne sont plus présents
		 cy.reload()
		 cy.get('table').contains('No vehicle available')
	 })
	 	 //ajouter 3 vehicules
	it('add 3 vehicls', () => {  
		cy.get('a[href="#configuration"]').click(); 
		 //premiere vehicle
        cy.get('input[name ="origin"][form="addVehicle"]').clear().type('5')
		cy.get('input[name ="destination"][form="addVehicle"]').clear().type('26')
		cy.get('input[name ="time"][form="addVehicle"]').clear().type('50')
		cy.get('form[name="addVehicle"] > button').click()
		cy.contains('Close').click()
		//deuxieme 
		cy.get('input[name ="origin"][form="addVehicle"]').clear().type('5')
		cy.get('input[name ="destination"][form="addVehicle"]').clear().type('26')
		cy.get('input[name ="time"][form="addVehicle"]').clear().type('80')
		cy.get('form[name="addVehicle"] > button').click()
		cy.contains('Close').click()
		//troisieme
		cy.get('input[name ="origin"][form="addVehicle"]').clear().type('5')
		cy.get('input[name ="destination"][form="addVehicle"]').clear().type('26')
		cy.get('input[name ="time"][form="addVehicle"]').clear().type('80')
		cy.get('form[name="addVehicle"] > button').click()
		cy.contains('Close').click()
	})
	it('edit time and execute simulation', () => { 
		 //changer temps et executer la simulation simultaion
		cy.contains('Simulation').click()
        cy.get('.form-control').clear().type('200')  
        cy.get('.btn').click();
		cy.wait(30000)

    })
})

